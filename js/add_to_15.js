var statusRef;
var numbers = [];
var game = true;
var player = [];
var computer = [];
var board = [1, 2, 3, 4, 5, 6, 7, 8, 9];
var wedge = 50;
var startx = 15;

var groups = [
    "  ",
    "3 4 8",
    "1 5 9",
    "2 6 7",
    "1 6 8",
    "3 5 7",
    "2 4 9",
    "2 5 8",
    "4 5 6"
];

var occupied = [
    [2, 4],
    [3, 6, 7],
    [1, 5],
    [1, 6, 8],
    [2, 5, 7, 8],
    [3, 4, 8],
    [3, 5],
    [1, 4, 7],
    [2, 6]
];

var pGroupCount = [0, 0, 0, 0, 0, 0, 0, 0, 0];
var cGroupCount = [0, 0, 0, 0, 0, 0, 0, 0, 0];

function init() {
    setUpBoard();
    statusRef = document.getElementById("status");
}

function smartChoice() {
    var boardl = board.length;
    for (var i = 0; i < boardl; i++) {
        var possible = board[i];
        for (var j = 0; j < occupied[possible - 1].length; j++) {
            if (cGroupCount[occupied[possible - 1][j]] == 2) {
                return (i);
            }
        }
    }
    for (var i = 0; i < boardl; i++) {
        var blocker = board[i];
        for (var j = 0; j < occupied[blocker - 1].length; j++) {
            if (pGroupCount[occupied[blocker - 1][j]] == 2) {
                return (i);
            }
        }
    }
    for (var i = 0; i < boardl; i++) {
        var possible = board[i];
        for (var j = 0; j < occupied[possible - 1].length; j++) {
            var whatgroup = occupied[possible - 1][j];
            if ((cGroupCount[whatgroup] == 1) && (pGroupCount[whatgroup] == 0)) {
                return (i);
            }
        }
    }
    for (var i = 0; i < boardl; i++) {
        if (board[i] == 5) {
            return (i);
        }
    }
    for (var i = 0; i < boardl; i++) {
        if (0 == board[i] % 2) {
            return (i);
        }
    }

    var ch = Math.floor(Math.random(0, boardl));
    return (ch);
}

function computerMove() {
    if (board.length < 1) {
        statusRef.innerHTML = "cat wins";
        return;
    }
    var which = smartChoice();
    var n = board[which];
    take(n);
    numbers[n - 1].style.top = "150px";
    numbers[n - 1].removeEventListener("click", addToPlayer);
    computer.push(n);

    var holder = occupied[n - 1];
    for (var i = 0; i < holder.length; i++) {
        cGroupCount[holder[i]]++;
        if (cGroupCount[holder[i]] == 3) {
            statusRef.innerHTML = "computer wins " + groups[holder[i]];
            game = false;
            return;
        }
    }
    if (board.length < 1) {
        statusRef.innerHTML = "cat wins";
    } else {
        game = true;
    }
}

function setUpBoard() {
    var dv;
    var xpos;
    for (var i = 1; i < 10; i++) {
        dv = document.createElement("span");
        dv.addEventListener("click", addToPlayer);
        dv.innerHTML = i.toString();
        xpos = startx + i * wedge;
        dv.style.left = xpos.toString() + "px";
        dv.style.top = "240px";
        document.body.appendChild(dv);
        dv.n = i;
        numbers.push(dv);
    }
}

function take(n) {
    var nAt = board.indexOf(n);
    if (nAt > -1) {
        board.splice(nAt, 1);
    }
}

function addToPlayer(event) {
    if (game) {
        var nn = event.target.n;
        event.target.removeEventListener("click", addToPlayer);
        player.push(nn);
        numbers[nn - 1].style.top = "350px";
        take(nn);
        var holder = occupied[nn - 1];
        for (i = 0; i < holder.length; i++) {
            pGroupCount[holder[i]]++;
            if (pGroupCount[holder[i]] == 3) {
                statusRef.innerHTML = "player wins " + groups[holder[i]];
                game = false;
                return;
            }
        }
        game = false;
        setTimeout(computerMove, 1000);
    } else {
        statusRef.innerHTML = "reload";
    }
}