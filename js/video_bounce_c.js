var ctx;
var cWidth;
var cHeight;
var ballRad = 50;
var maskRad;
var ballx = 50;
var bally = 60;
var canvas1;
var ballvx = 2;
var ballvy = 4;
var v;
var videow;
var videoh;

function init() {
    canvas1 = document.getElementById('canvas');
    ctx = canvas1.getContext('2d');
    canvas1.width = window.innerWidth;
    cWidth = canvas1.width;
    canvas1.height = window.innerHeight;
    cHeight = canvas1.height;
    v = document.getElementById('vid');
    var aspect = v.videoWidth / v.videoHeight;
    v.width = Math.min(v.videoWidth, .5 * cWidth);
    v.height = v.width / aspect;
    v.height = Math.min(v.height, .5 * cHeight);
    v.width = aspect * v.height;
    window.onscroll = function () {
        window.scrollTo(0, 0);
    };
    videow = v.width;
    videoh = v.height;
    ballRad = Math.min(.5 * videow, .5 * videoh);
    maskRad = .4 * Math.min(videow, videoh);
    ctx.lineWidth = ballRad;
    ctx.strokeStyle = "rgb(200, 0, 50)";
    ctx.fillStyle = "white";
    v.play();
    setInterval(drawScene, 50);
}

function drawScene() {
    ctx.clearRect(0, 0, cWidth, cHeight);
    checkPosition();
    ctx.drawImage(v, ballx, bally, videow, videoh);
    ctx.beginPath();
    ctx.moveTo(ballx, bally);
    ctx.lineTo(ballx + videow + 2, bally);
    ctx.lineTo(ballx + videow + 2, bally + .5 * videoh + 2);
    ctx.lineTo(ballx + .5 * videow + ballRad, bally + .5 * videoh + 2);
    ctx.arc(ballx + .5 * videow, bally + .5 * videoh, ballRad, 0, Math.PI, true);
    ctx.lineTo(ballx, bally + .5 * videoh);
    ctx.lineTo(ballx, bally);
    ctx.fill();
    ctx.closePath();
    ctx.beginPath();
    ctx.moveTo(ballx, bally + .5 * v.height);
    ctx.lineTo(ballx, bally + v.height);
    ctx.lineTo(ballx + videow + 2, bally + videoh);
    ctx.lineTo(ballx + videow + 2, bally + .5 * videoh - 2);
    ctx.lineTo(ballx + .5 * videow + ballRad, bally + .5 * videoh - 2);
    ctx.arc(ballx + .5 * videow, bally + .5 * videoHeight, ballRad, 0, Math.PI, false);
    ctx.lineTo(ballx, bally + .5 * videoh);
    ctx.fill();
    ctx.closePath();
    ctx.strokeRect(0, 0, cWidth, cHeight);
}

function checkPosition() {
    var nballx = ballx + ballvx + .5 * videow;
    var nbally = bally + ballvy + .5 * videoh;

    if (nballx > cWidth) {
        ballvx = -ballvx;
        nballx = cWidth;
    }

    if (nballx < 0) {
        nballx = 0;
        ballvx = -ballvx;
    }

    if (nbally > cHeight) {
        nbally = cHeight;
        ballvy = -ballvy;
    }

    if (nbally < 0) {
        nbally = 0;
        ballvy = -ballvy;
    }
    ballx = nballx - .5 * videow;
    bally = nbally - .5 * videoh;
}

function reverse() {
    ballvx = -ballvx;
    ballvy = -ballvy;
}