var pieces = [];
var nums;
var baseImgW;
var baseImgH;
var origW;
var origH;
var opieceW;
var opieceH;
var pieceW;
var pieceH;
var numOfRows = 2.0;
var numOfCols = 3.0;
var piecesx = [];
var piecesy = [];
var v;
var base;
var doingjigsaw = false;
var firstpkel;
var oldx;
var oldy;
var questionfel;
var movingobj;

function init() {

    v = document.getElementById("bars");
    base = document.getElementById("base");
    makePieces();
    nums = pieces.length;
    questionfel = document.getElementById("questionform");
    questionfel.style.left = "20px";
    questionfel.style.top = "600px";
    questionfel.submitbut.value = "Do jigsaw again.";
    setupGame();
}

function makePieces() {
    var i;
    var x;
    var y;
    var s;
    var sCTX;
    origW = base.width;
    origH = base.height;
    var ratio = Math.min(1.0, .80 * window.innerWidth / origW, .80 * window.innerHeight / origH);
    baseImgW = origW * ratio;
    baseImgH = origH * ratio;
    v.width = baseImgW;
    v.height = baseImgH;
    opieceW = origW / numOfCols;
    opieceH = origH / numOfRows;
    pieceW = ratio * opieceW;
    pieceH = ratio * opieceH;

    for (i = 0.0; i < numOfRows; i++) {
        for (j = 0.0; j < numOfCols; j++) {
            s = document.createElement('canvas');
            s.width = pieceW;
            s.height = pieceH;
            s.style.position = 'absolute';
            sCTX = s.getContext('2d');

            sCTX.drawImage(base, j * opieceW, i * opieceH, opieceW, opieceH, 0, 0, pieceW, pieceH);

            document.body.appendChild(s);

            pieces.push(s);
            x = j * pieceW + 100;
            y = i * pieceH + 100;
            s.style.top = String(y) + "px";
            s.style.left = String(x) + "px";
            piecesx.push(x);
            piecesy.push(y);
            s.addEventListener('mousedown', startdragging);

            s.style.visibility = 'visible';
        }
    }
    firstpkel = pieces[0];
    document.body.onmouseup = release;
}

function endjigsaw() {
    if (doingjigsaw) {
        doingjigsaw = false;
        v.pause();
        v.style.display = "none";
    }
    setupGame();
    return false;
}

function checkpositions() {
    var i;
    var x;
    var y;
    var tolerance = 10;
    var deltax = [];
    var deltay = [];
    var delx;
    var dely;
    for (i = 0; i < nums; i++) {
        x = pieces[i].style.left;
        y = pieces[i].style.top;
        x = x.substr(0, x.length - 2);
        y = y.substr(0, y.length - 2);
        x = Number(x);
        y = Number(y);
        delx = x - piecesx[i];
        dely = y - piecesy[i];
        deltax.push(delx);
        deltay.push(dely);
    }
    var averagex = doaverage(deltax);
    var averagey = doaverage(deltay);
    for (i = 0; i < nums; i++) {
        if ((Math.abs(averagex - deltax[i]) > tolerance) || (Math.abs(averagey - deltay[i]) > tolerance)) {
            break;
        }
    }
    if (i < nums) {
        questionfel.feedback.value = "Keep working.";
    } else {

        questionfel.feedback.value = "GOOD!";
        for (i = 0; i < nums; i++) {
            pieces[i].style.display = "none";

        }
        v.style.left = firstpkel.style.left;
        v.style.top = firstpkel.style.top;
        v.style.display = "block";
        v.currentTime = 0;
        v.play();
    }
}

function doaverage(arr) {
    var sum;
    var i;
    var n = arr.length;
    sum = 0;
    for (i = 0; i < n; i++) {
        sum += arr[i];
    }
    return (sum / n);
}

function setupGame() {
    var i;
    var x;
    var y;
    var thingelem;
    v.pause();
    v.style.display = "none";
    doingjigsaw = true;
    for (i = 0; i < nums; i++) {
        x = 10 + Math.floor(Math.random() * baseImgW * .9);
        y = 50 + Math.floor(Math.random() * baseImgH * .9);
        thingelem = pieces[i];
        thingelem.style.top = String(y) + "px";
        thingelem.style.left = String(x) + "px";
        thingelem.style.visibility = 'visible';
        thingelem.style.display = "inline";
    }
    questionfel.feedback.value = "  ";
}

function release(e) {
    movingobj = e.release;
    mouseDown = false;
    movingobj.removeEventListener("mousemove", moving);
    movingobj.removeEventListener("mouseup", release);
    movingobj = null;

    checkpositions();
}


function startdragging(e) {
    movingobj = e.target;

    mouseDown = true;
    oldx = parseInt(e.pageX);
    oldy = parseInt(e.pageY);
    movingobj.addEventListener("mousemove", moving);
    movingobj.addEventListener("mouseup", release);
}

function moving(ev) {
    if ((movingobj != null) && (mouseDown)) {
        newx = parseInt(ev.pageX);
        newy = parseInt(ev.pageY);
        delx = newx - oldx;
        dely = newy - oldy;
        oldx = newx;
        oldy = newy;
        curx = parseInt(movingobj.style.left);
        cury = parseInt(movingobj.style.top);
        movingobj.style.left = String(curx + delx) + "px";
        movingobj.style.top = String(cury + dely) + "px";
    }
}