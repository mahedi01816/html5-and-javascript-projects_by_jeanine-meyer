var ctx;
var canvas1;
var stuff = [];
var thingInMotion;
var offsetx;
var offsety;
var tid;
var savedgco;
var images = [];
var videotext1 = "<video id=\"XXXX\"  preload=\"auto\" loop=\"loop\" autoplay muted controls> <source src=\"XXXX.webm\" " +
    "type=\'video/webm; codecs=\"vp8, vorbis\"\'> ";
var videotext2 = "<source src=\"XXXX.mp4\" type=\'video/mp4; codecs=\"avc1.42E01E, mp4a.40.2\"\'> " +
    "<source src=\"XXXX.ogv\" type=\'video/ogg; codecs=\"theora, vorbis\"\'>";
var videotext3 = "Your browser does not accept the video tag.</video>";

function restart(ev) {
    var v = ev.target;
    v.currentTime = 0;
    v.play();
}

var videocount = 0;
var okaytogo = false;

function videoLoaded(ev) {
    ctx.fillText(ev.target.id + " loaded.", 400, 100 * videocount);
    ev.target.play();
    videocount--;
    if (videocount == 0) {
        okaytogo = true;
    }
}

var textmsg = "Loading videos";

function init() {
    canvas1 = document.getElementById('canvas');
    canvas1.onmousedown = function () {
        return false;
    };
    canvas1.addEventListener('dblclick', makeNewItem, false);
    canvas1.addEventListener('mousedown', startDragging, false);
    ctx = canvas1.getContext('2d');
    savedgco = ctx.globalCompositeOperation;
    createElements();
    drawStuff();
    ctx.fillText(textmsg, 100, 100);
    loadId = setInterval(loading, 2000);
    ctx.strokeStyle = "blue";
}

function loading() {
    if (okaytogo) {
        clearInterval(loadId);
        tid = setInterval(drawStuff, 40);
    } else {
        textmsg += ".";
        ctx.fillText(textmsg, 100, 100);
    }
}

function createElements() {
    var name;
    var i;
    var type;
    var divElement;
    var videomarkup;
    var velref;
    var vb;
    var imgdummy;
    //var pic;

    for (i = 0; i < mediaInfo.length; i++) {
        type = mediaInfo[i].shift();
        info = mediaInfo[i];

        switch (type) {
            case 'video':
                videocount++;
                name = info[0];
                divElement = document.createElement("div");
                videomarkup = videotext1 + videotext2 + videotext3;
                videomarkup = videomarkup.replace(/XXXX/g, name);
                divElement.innerHTML = videomarkup;
                document.body.appendChild(divElement);
                velref = document.getElementById(name);
                velref.addEventListener("ended", restart, false);
                velref.addEventListener("loadeddata", videoLoaded, false);
                vb = new VideoBlock(info[2], info[3], info[4], info[5], info[6], info[7], info[8], velref, info[9],
                    info[1], info[10]);
                stuff.push(vb);
                break;
            case 'picture':
                imgdummy = new Image();
                imgdummy.src = info[4];
                images.push(imgdummy);
                stuff.push(new Picture(info[0], info[1], info[2], info[3], images[images.length - 1]));
                break;
            case 'heart':
                stuff.push(new Heart(info[0], info[1], info[2], info[3], info[4]));
                break;
            case 'oval':
                stuff.push(new Oval(info[0], info[1], info[2], info[3], info[4], info[5]));
                break;
            case 'rect':
                stuff.push(new Rect(info[0], info[1], info[2], info[3], info[4]));
                break;
        }
    }
}

function distsq(x1, y1, x2, y2) {
    var xd = x1 - x2;
    var yd = y1 - y2;
    return ((xd * xd) + (yd * yd));
}

function VideoBlock(sx, sy, x, y, w, h, scale, videoel, volume, angle, alpha) {
    this.sx = sx;
    this.sy = sy;
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.videoElement = videoel;
    this.volume = volume;
    this.draw = drawVideo;
    this.overCheck = overVideo;
    this.angle = angle;
    this.cosine = Math.cos(angle);
    this.sine = Math.sin(angle);
    this.scale = scale;
    this.alpha = alpha;
    videoel.volume = 0;
    this.videoElement.play();
}

function overVideo(mx, my) {
    omx = mx;
    omy = my;
    if (this.angle != 0) {
        omx = omx - this.x;
        omy = omy - this.y;
        mx = omx * this.cosine + omy * this.sine;
        my = -omx * this.sine + omy * this.cosine;
        mx = this.x + mx;
        my = this.y + my;
    }
    if (this.scale != 1) {
        mx = mx / this.scale;
        my = my / this.scale;
    }
    if (((mx >= this.x) && (mx <= (this.x + this.w)) && (my >= this.y) && (my <= this.y + this.h))) {
        return true;
    } else {
        return false;
    }
}

function drawVideo() {
    var savedAlpha = ctx.globalAlpha;
    ctx.globalCompositeOperation = "lighter";
    ctx.globalAlpha = this.alpha;
    if (this.angle != 0) {
        ctx.save();
        ctx.translate(this.x, this.y);
        ctx.rotate(this.angle);
        ctx.translate(-this.x, -this.y);
        if (this.scale != 1) {
            ctx.scale(this.scale, this.scale);
        }
        ctx.drawImage(this.videoElement, this.sx, this.sy, this.w, this.h, this.x, this.y, this.w, this.h);
        ctx.restore();
    } else {
        if (this.scale != 1) {
            ctx.save();
            ctx.scale(this.scale, this.scale);
            ctx.drawImage(this.videoElement, this.sx, this.sy, this.w, this.h, this.x, this.y, this.w, this.h);
            ctx.restore();
        } else {
            ctx.drawImage(this.videoElement, this.sx, this.sy, this.w, this.h, this.x, this.y, this.w, this.h);
        }
    }
    ctx.globalAlpha = savedAlpha;
    ctx.globalCompositeOperation = savedgco;
}

function Picture(x, y, w, h, imageName) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.imageName = imageName;
    this.draw = drawPic;
    this.overCheck = overRect;
}

function Heart(x, y, h, drx, color) {
    this.x = x;
    this.y = y;
    this.h = h;
    this.drx = drx;
    this.radsq = drx * drx;
    this.color = color;
    this.draw = drawHeart;
    this.overCheck = overHeart;
    this.ang = .25 * Math.PI;
}

function drawHeart() {
    var leftctrx = this.x - this.drx;
    var rightctrx = this.x + this.drx;
    var cx = rightctrx + this.drx * Math.cos(this.ang);
    var cy = this.y + this.drx * Math.sin(this.ang);
    ctx.fillStyle = this.color;
    ctx.beginPath();
    ctx.moveTo(this.x, this.y);
    ctx.arc(leftctrx, this.y, this.drx, 0, Math.PI - this.ang, true);
    ctx.lineTo(this.x, this.y + this.h);
    ctx.lineTo(cx, cy);
    ctx.arc(rightctrx, this.y, this.drx, this.ang, Math.PI, true);
    ctx.closePath();
    ctx.fill();
}

function overHeart(mx, my) {
    var leftctrx = this.x - this.drx;
    var rightctrx = this.x + this.drx;
    var qx = this.x - 2 * this.drx;
    var qy = this.y - this.drx;
    var qwidth = 4 * this.drx;
    var qheight = this.drx + this.h;
    if (outSide(qx, qy, qwidth, qheight, mx, my)) {
        return false;
    }
    if (distsq(mx, my, leftctrx, this.y) < this.radsq) {
        return true;
    }
    if (distsq(mx, my, rightctrx, this.y) < this.radsq) {
        return true;
    }
    if (my <= this.y) {
        return false;
    }
    var x2 = this.x;
    var y2 = this.y + this.h;
    var m = (this.h) / (2 * this.drx);
    if (mx <= this.x) {
        if (my < (m * (mx - x2) + y2)) {
            return true;
        } else {
            return false;
        }
    } else {
        m = -m;
    }
    if (my < (m * (mx - x2) + y2)) {
        return true;
    } else {
        return false;
    }
}

function outSide(x, y, w, h, mx, my) {
    return ((mx < x) || (mx > (x + w)) || (my < y) || (my > (y + h)));
}

function drawPic() {
    ctx.globalAlpha = 1.0;
    ctx.drawImage(this.imageName, this.x, this.y, this.w, this.h);
}

function Oval(x, y, r, hor, ver, c) {
    this.x = x;
    this.y = y;
    this.r = r;
    this.radsq = r * r;
    this.hor = hor;
    this.ver = ver;
    this.draw = drawOval;
    this.color = c;
    this.overCheck = overOval;
}

function drawOval() {
    ctx.save();
    ctx.translate(this.x, this.y);
    ctx.scale(this.hor, this.ver);
    ctx.fillStyle = this.color;
    ctx.beginPath();
    ctx.arc(0, 0, this.r, 0, 2 * Math.PI, true);
    ctx.closePath();
    ctx.fill();
    ctx.restore();
}

function Rect(x, y, w, h, c) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.draw = drawRect;
    this.color = c;
    this.overCheck = overRect;
}

function overOval(mx, my) {
    var x1 = 0;
    var y1 = 0;
    var x2 = (mx - this.x) / this.hor;
    var y2 = (my - this.y) / this.ver;
    if (distsq(x1, y1, x2, y2) <= (this.radsq)) {
        return true;
    } else {
        return false;
    }
}

function overRect(mx, my) {
    if ((mx >= this.x) && (mx <= (this.x + this.w)) && (my >= this.y) && (my <= (this.y + this.h))) {
        return true;
    } else {
        return false;
    }
}

function makeNewItem(ev) {
    var mx;
    var my;
    if (ev.layerX || ev.layerX == 0) {
        mx = ev.layerX;
        my = ev.layerY;
    } else if (ev.offsetX || ev.offsetX == 0) {
        mx = ev.offsetX;
        my = ev.offsetY;
    }
    var endpt = stuff.length - 1;
    var item;
    for (var i = endpt; i >= 0; i--) {
        if (stuff[i].overCheck(mx, my)) {
            item = clone(stuff[i]);
            item.x += 20;
            item.y += 20;
            stuff.push(item);
            break;
        }
    }
}

function clone(object) {
    var item = new Object();
    for (var info in object) {
        item[info] = object[info];
    }
    return item;
}

function startDragging(event) {
    var mx;
    var my;
    if (event.layerX || event.layerX == 0) {
        mx = event.layerX;
        my = event.layerY;
    } else if (event.offsetX || event.offsetX == 0) {
        mx = event.offsetX;
        my = event.offsetY;
    }
    var endpt = stuff.length - 1;
    for (var i = endpt; i >= 0; i--) {
        if (stuff[i].overCheck(mx, my)) {
            offsetx = mx - stuff[i].x;
            offsety = my - stuff[i].y;
            var item = stuff[i];
            thingInMotion = stuff.length - 1;
            stuff.splice(i, 1);
            stuff.push(item);
            canvas1.style.cursor = "pointer";
            canvas1.addEventListener('mousemove', moveIt, false);
            canvas1.addEventListener('mouseup', dropIt, false);
            break;
        }
    }
}

function dropIt(event) {
    canvas1.removeEventListener('mousemove', moveIt, false);
    canvas1.removeEventListener('mouseup', dropIt, false);
    canvas1.style.cursor = "crosshair";
}

function moveIt(event) {
    var mx;
    var my;
    if (event.layerX || event.layerX == 0) {
        mx = event.layerX;
        my = event.layerY;
    } else if (event.offsetX || event.offsetX == 0) {
        mx = event.offsetX;
        my = event.offsetY;
    }
    stuff[thingInMotion].x = mx - offsetx;
    stuff[thingInMotion].y = my - offsety;
}

function drawStuff() {
    ctx.clearRect(0, 0, 800, 600);
    ctx.strokeStyle = "black";
    ctx.lineWidth = 2;
    ctx.strokeRect(0, 0, 800, 600);
    for (var i = 0; i < stuff.length; i++) {
        stuff[i].draw();
    }
}

function drawRect() {
    ctx.fillStyle = this.color;
    ctx.fillRect(this.x, this.y, this.w, this.h);
}

function saveAsImage() {
    try {
        window.open(canvas1.toDataURL("image/png"));
    } catch (e) {
        alert("You need to change browser");
    }
}

function removeObject() {
    stuff.pop();
    drawStuff();
}