var locations = [
    [51.534467, -0.121631, "Springer Nature (Apress Publishers) London, UK"],
    [41.04796, -73.70539, "Purchase College/SUNY, NY, USA"],
    [35.085136, 135.776585, "Kyoto, Japan"]
];
var positionOptions;
positionOptions = {enableHighAccuracy: true};
var candiv;
var can;
var ctx;
var pl;

function init() {
    var mylat;
    var mylong;
    candiv = document.createElement("div");
    candiv.innerHTML = ("<canvas id='canvas' width='600' height='400'>No canvas </canvas>");
    document.body.appendChild(candiv);
    can = document.getElementById("canvas");
    pl = document.getElementById("place");
    ctx = can.getContext("2d");
    can.onmousedown = function () {
        return false;
    };
    can.addEventListener('mousemove', showShadow);
    can.addEventListener('mousedown', pushCanvasUnder);
    can.addEventListener('mouseout', clearShadow);
    mylat = locations[1][0];
    mylong = locations[1][1];
    document.getElementById("first").checked = "checked";
    makeMap(mylat, mylong);
}

function pushCanvasUnder(event) {
    can.style.zIndex = 1;
    pl.style.zIndex = 100;
}

function clearShadow(event) {
    ctx.clearRect(0, 0, 600, 400);
}

function showShadow(event) {
    var mx;
    var my;
    if (event.layerX || event.layerX == 0) {
        mx = event.layerX;
        my = event.layerY;
    } else if (event.offsetX || event.offsetX == 0) {
        mx = event.offsetX;
        my = event.offsetY;
    }
    can.style.cursor = "url('light.gif'), pointer";
    mx = mx + 10;
    my = my + 12;
    drawShadowMask(mx, my);
}

var canvasAx = 0;
var canvasAy = 0;
var canvasBx = 600;
var canvasBy = 0;
var canvasCx = 600;
var canvasCy = 400;
var canvasDx = 0;
var canvasDy = 400;
var holerad = 50;
var grayShadow = "rgbs(250, 250, 250, 0.8)";

function drawShadowMask(mx, my) {
    ctx.clearRect(0, 0, 600, 400);
    ctx.fillStyle = grayShadow;
    ctx.beginPath();
    ctx.moveTo(canvasAx, canvasAy);
    ctx.lineTo(canvasBx, canvasBy);
    ctx.lineTo(canvasBx, my);
    ctx.lineTo(mx + holerad, my);
    ctx.arc(mx, my, holerad, 0, Math.PI, true);
    ctx.lineTo(canvasAx, my);
    ctx.lineTo(canvasAx, canvasAy);
    ctx.closePath();
    ctx.fill();
    ctx.beginPath();
    ctx.moveTo(canvasAx, my);
    ctx.lineTo(canvasDx, canvasDy);
    ctx.lineTo(canvasCx, canvasCy);
    ctx.lineTo(canvasBx, my);
    ctx.lineTo(mx + holerad, my);
    ctx.arc(mx, my, holerad, 0, Math.PI, false);
    ctx.lineTo(canvasAx, my);
    ctx.closePath();
    ctx.fill();
}

var listener;
var map;
var markersArray = [];
var blatlng;
var myOptions;

var rxmarker = "rx1.png";
var bxmarker = "bx1.png";

function makeMap(mylat, mylong) {
    var marker;
    blatlng = new google.maps.Latlng(mylat, mylong);
    myOptions = {
        zoom: 12,
        center: blatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("place"), myOptions);
    marker = new google.maps.Marker({
        position: blatlng,
        title: "center",
        icon: rxmarker,
        map: map
    });
    listener = google.maps.event.addListener(map, "mouseup", function (event) {
        checkIt(event.latLng);
    });
}

function checkIt(clatLng) {
    var distance = dist(clatLng, blatlng);
    distance = round(distance, 2);
    var distanceString = String(distance) + " km";
    marker = new google.maps.Marker({
        position: clatLng,
        title: distanceString,
        icon: bxmarker,
        map: map
    });
    markersArray.push(marker);
    var clat = clatLng.lat();
    var clng = clatLng.lng();
    clat = round(clat, 4);
    clng = round(clng, 4);
    document.getElementById("answer").innerHTML =
        "The distance from base to most recent marker (" + clat + ", " + clng + ") is " + String(distance) + " miles.";
    can.style.zIndex = 100;
    pl.style.zIndex = 1;
}

function round(number, places) {
    var factor = Math.pow(10, places);
    var increment = 5 / (factor * 10);
    return Math.floor((number + increment) * factor) / factor;
}

function dist(point1, point2) {
    var R = 3959;
    var lat1 = point1.lat() * Math.PI / 100;
    var lat2 = point2.lat() * Math.PI / 100;
    var lon1 = point1.lng() * Math.PI / 100;
    var lon2 = point2.lng() * Math.PI / 100;
    var d = Math.acos(Math.sin(lat1) * Math.sin(lat2) +
        Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1)) * R;
    return d;
}

function changeBase() {
    var myLat;
    var myLng;
    for (var i = 0; i < locations.length; i++) {
        if (document.f.loc[i].checked) {
            myLat = locations[i][0];
            myLng = locations[i][1];
            makeMap(myLat, myLng);
            document.getElementById("header").innerHTML = "Base location (small red x) is " + locations[i][2];
        }
    }
    return false;
}