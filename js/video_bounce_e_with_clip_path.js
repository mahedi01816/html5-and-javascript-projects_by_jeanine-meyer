var ctx;
var cWidth;
var cHeight;
var ballRad = 50;
var ballx = 80;
var bally = 80;
var ballvx = 2;
var ballvy = 4;
var v;
var videow;
var videoh;

function init() {
    canvas1 = document.getElementById('canvas');
    ctx = canvas1.getContext('2d');
    canvas1.width = window.innerWidth;
    cWidth = canvas1.width;
    canvas1.height = window.innerHeight;
    cHeight = canvas1.height;
    window.onscroll = function () {
        window.scrollTo(0, 0);
    };
    v = document.getElementById('vid');
    var aspect = v.videoWidth / v.videoHeight;
    v.width = Math.min(v.videoWidth, .5 * cWidth);
    v.height = v.width / aspect;
    v.height = Math.min(v.height, .5 * cHeight);
    v.width = aspect * v.height;
    videow = v.width;
    videoh = v.height;

    amt = .5 * Math.min(videow, videoh);
    amtS = String(amt) + "px";
    v.style.clipPath = "circle(" + amtS + " at center)";

    ballRad = Math.min(.5 * videow, .5 * videoh);
    ctx.lineWidth = ballRad;
    ctx.strokeStyle = "rgb(200, 0, 50)";
    ctx.fillStyle = "white";

    v.style.left = String(ballx) + "px";
    v.style.top = String(bally) + "px";

    v.play();
    v.style.visibility = "visible";
    v.style.display = "block";
    ctx.strokeRect(0, 0, cWidth, cHeight);
    setInterval(moveVideo, 50);
}

function moveVideo() {
    checkPosition();
    v.style.left = String(ballx) + "px";
    v.style.top = String(bally) + "px";
}

function checkPosition() {
    var nballx = ballx + ballvx;
    var nbally = bally + ballvy;

    if ((nballx + videow) > cWidth) {
        ballvx = -ballvx;
        nballx = cWidth - videow;
    }

    if (nballx < 0) {
        nballx = 0;
        ballvx = -ballvx;
    }

    if ((nbally + videoh) > cHeight) {
        nbally = cHeight - videoh;
        ballvy = -ballvy;
    }

    if (nbally < 0) {
        nbally = 0;
        ballvy = -ballvy;
    }
    ballx = nballx;
    bally = nbally;
}

function reverse() {
    ballvx = -ballvx;
    ballvy = -ballvy;
}