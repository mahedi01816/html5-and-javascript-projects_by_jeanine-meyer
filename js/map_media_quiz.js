var listener;
var map;

var myOptions;
var ctx;
var bLatLng;
var content = [];

var answer;
var v;
var audioEl;

var videoText1 = "<video id=\"XXXX\" loop=\"loop\" preload=\"auto\" controls=\"controls\" width=\"400\"><source " +
    "src=\"XXXX.webmpv8.webm\" type=\'video/webm\'>";
var videoText2 = "<source src=\"XXXX.theora.ogv\" type=\'video/ogg\'>  <source src=\"XXXX.mp4\"  type=\'video/mp4\'>";
var videoText3 = "Your browser does not accept the video tag.</video>";
var audioText1 = "<audio id=\"XXXX\" controls=\"controls\" preload=\"preload\"><source src=\"XXXX.ogg\" " +
    "type=\"audio/ogg\" />";
var audioText2 = "<source src=\"XXXX.mp3\" type=\"audio/mpeg\" /><source src=\"XXXX.wav\" type=\"audio/wav\" /></audio>";
var nextQuestion = -1;

function init() {
    ctx = document.getElementById("canvas").getContext("2d");
    answer = document.getElementById("answer");
    header = document.getElementById("header");
    loadContent();
    askNewQuestion();
}

function askNewQuestion() {
    nextQuestion++;
    if (nextQuestion < questions.length) {
        header.innerHTML = questions[nextQuestion];
    } else {
        header.innerHTML = "No more questions."
    }
}

function loadContent() {
    var divElement;
    makeMap(base[0], base[1]);
    var videoMarkup;
    var videoReference;
    var audioMarkup;
    var audioReference;
    var imageObj;
    var name;
    var savedImageFileName;

    for (var i = 0; i < precontent.length; i++) {
        content.push(precontent[i]);
        name = precontent[i][4];
        switch (precontent[i][3]) {
            case "video" :
                divElement = document.createElement("div");
                videoMarkup = videoText1 + videoText2 + videoText3;
                videoMarkup = videoMarkup.replace(/XXXX/g, name);
                divElement.innerHTML = videoMarkup;
                document.body.appendChild(divElement);
                videoReference = document.getElementById(name);
                content[i][4] = videoReference;
                break;
            case "pictureaudio" :
                divElement = document.createElement("div");
                audioMarkup = audioText1 + audioText2;
                audioMarkup = audioMarkup.replace(/XXXX/g, name);
                divElement.innerHTML = audioMarkup;
                document.body.appendChild(divElement);
                audioReference = document.getElementById(name);
                savedImageFileName = content[i][5];
                content[i][5] = audioReference;
                imageObj = new Image();
                imageObj.src = savedImageFileName;
                content[i][4] = imageObj;
                break;
            case "picture" :
                imageObj = new Image();
                imageObj.src = precontent[i][4];
                content[i][4] = imageObj;
                break;
        }
    }
}

var rxMarker = "rx1.png";
var bxMarker = "bx1.png";

function makeMap(myLat, myLng) {
    var marker;
    bLatLng = new google.maps.LatLng(myLat, myLng);
    myOptions = {
        zoom: zoomlevel,
        center: bLatLng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("place"), myOptions);
    marker = new google.maps.Marker({
        position: bLatLng,
        title: "center",
        icon: rxMarker,
        map: map
    });
    listener = google.maps.event.addListener(map, "click", function (event) {
        checkIt(event.latlng);
    })
}

function eraseOld() {
    if (v != undefined) {
        v.pause();
        v.style.display = "none";
    }
    if (audioEl != undefined) {
        audioEl.pause();
        audioEl.style.display = "none";
    }
    ctx.clearRect(0, 0, 300, 400);
}

function checkIt(clatlng) {
    var marker;
    var latlng = new google.maps.LatLng(content[nextQuestion][0], content[nextQuestion][1]);
    var distance = dist(clatlng, latlng);
    eraseOld();
    marker = new google.maps.Marker({
        position: clatlng,
        title: "Your answer",
        icon: bxMarker,
        map: map
    });
    if (distance < maxDistance) {
        switch (content[nextQuestion][3]) {
            case "video" :
                answer.innerHTML = content[nextQuestion][2];
                ctx.clearRect(0, 0, 400, 400);
                v = content[nextQuestion][4];
                v.style.display = "block";
                v.currentTime = 0;
                v.play();
                break;
            case "picture" :
            case "pictureaudio" :
                answer.innerHTML = content[nextQuestion][2];
                ctx.clearRect(0, 0, 400, 400);
                var img1 = content[nextQuestion][4];
                var iw = img1.width;
                var ih = img1.height;
                var aspect = iw / ih;
                if (iw >= ih) {
                    if (iw > 400) {
                        tw = 400;
                        th = 400 / aspect;
                    } else {
                        tw = iw;
                        th = ih;
                    }
                } else {
                    if (th > 400) {
                        th = 400;
                        tw = 400 * aspect;
                    } else {
                        th = ih;
                        tw = iw;
                    }
                }
                ctx.drawImage(img1, 0, 0, iw, ih, 0, 0, tw, th);
                if (content[nextQuestion][3] == "picture") {
                    break;
                } else {
                    audioEl = content[nextQuestion][5];
                    audioEl.style.display = "block";
                    audioEl.currentTime = 0;
                    audioEl.play();
                    break;
                }
        }
        askNewQuestion();
    } else {
        answer.innerHTML = "not";
    }
}

function dist(point1, point2) {
    var R = 6371;
    var lat1 = point1.lat() * Math.PI / 100;
    var lat2 = point2.lat() * Math.PI / 100;
    var lon1 = point1.lng() * Math.PI / 100;
    var lon2 = point2.lng() * Math.PI / 100;

    var d = Math.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1)) * R;
    return d;
}

function giveUp() {
    makeMap(content[nextQuestion][0], content[nextQuestion][1]);
    eraseOld();
    answer.innerHTML = "click";
}